package com.enriquez;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("** Control Structures **");

        // Control Structures

        int num1 = 10;
        int num2 = 200;

        // If Statement
        if (num1 > 5) {
            System.out.println("num1 is greater than 5");
        }

        // if...else Statement
        if(num2 > 100)
            System.out.println("num2 is greater than 100");
        else
            System.out.println("num2 is less than 100");

        // String Comparison
        String word = "hello";

        if (word == "hello")
            System.out.println("hi");

        if (word.equals("hello"))
            System.out.println("hi");

        // Logical Operators and Short Circuiting
        // and = &&, or = ||, not = !
        int x = 15;
        int y = 0;

        if (y > 5 && x/y == 0)
            System.out.println("Result is " + x/y);
        else
            System.out.print("Short circuited");



    }
}
